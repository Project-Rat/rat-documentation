cd ../../
cd rat-common/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd materials-cpp/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd materials-data/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd distmesh-cpp/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-math/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-mlfmm/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-nl/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_CHOLMOD=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-models/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_NL_SOLVER=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd raccoon2/build
git checkout dev
git pull origin dev
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-gui/build
git checkout dev
git pull origin dev
# git submodule update --recursive --remote
# cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_TOKAMAK_ENERGY=ON -DENABLE_CERN=ON -DENABLE_RACCOON=ON -DENABLE_KEYGEN=OFF -DENABLE_BETA=ON ..
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_TOKAMAK_ENERGY=OFF -DENABLE_CERN=OFF -DENABLE_RACCOON=OFF -DENABLE_KEYGEN=ON -DENABLE_BETA=OFF ..
make -j128
#CTEST_PARALLEL_LEVEL=16 make test
cd ../../
cd rat-documentation/shell


