cd ../../
cd rat-common
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-math
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd materials-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd materials-data
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd distmesh-cpp
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-mlfmm
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-nl
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_CHOLMOD=ON -DENABLE_CHOLMOD_GPL=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-models
git checkout dev
git pull origin dev
sudo rm -r build
# sudo rm -r release
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_NL_SOLVER=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd raccoon2
git checkout dev
git pull origin dev
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

cd rat-gui
git checkout dev
git pull origin dev
# git submodule update --recursive --remote
sudo rm -r build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_TOKAMAK_ENERGY=OFF -DENABLE_RACCOON=OFF -DENABLE_KEYGEN=ON -DENABLE_BETA=OFF ..
make -j128
# CTEST_PARALLEL_LEVEL=16 make test
cd ../../

cd rat-documentation/shell
