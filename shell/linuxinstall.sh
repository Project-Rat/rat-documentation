cd ../../
git clone git@gitlab.com:Project-Rat/rat-common.git
cd rat-common
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-math.git
cd rat-math
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/distmesh-cpp.git
cd distmesh-cpp
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/materials-cpp.git
cd materials-cpp
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/materials-data.git
cd materials-data
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-mlfmm.git
cd rat-mlfmm
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-nl.git
cd rat-nl
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_CHOLMOD=ON -DENABLE_CHOLMOD_GPL=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-models.git
cd rat-models
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_NL_SOLVER=ON ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Raccoon/raccoon2.git
cd raccoon2
git checkout dev
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j128
CTEST_PARALLEL_LEVEL=16 make test
sudo make install
cd ../../

git clone git@gitlab.com:Project-Rat/rat-gui.git
cd rat-gui
git checkout dev
git submodule update --init --remote --recursive
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_TOKAMAK_ENERGY=OFF -DENABLE_RACCOON=OFF -DENABLE_KEYGEN=ON -DENABLE_BETA=OFF ..
make -j128
#CTEST_PARALLEL_LEVEL=16 make test
cd ../../

cd rat-documentation/shell
