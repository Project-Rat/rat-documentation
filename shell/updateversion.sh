VERSION_TAG="v2.018.5"
directories=(
	"../../rat-common/build" 
	"../../distmesh-cpp/build" 
	"../../materials-cpp/build"
	"../../materials-data/build"
	"../../rat-math/build"
	"../../rat-mlfmm/build"
	"../../rat-nl/build"
	"../../rat-models/build"
	"../../raccoon2/build"
	"../../rat-gui/build"
	"../../rat-template/build"
	)

for dir in "${directories[@]}"
do
	cd "$dir" &&
	git config pull.rebase false &&
	git checkout dev &&
	git pull origin dev &&
	git add .. &&
	git commit -m "Update version number." &&
	git push origin dev &&
	git checkout master &&
	git pull origin master &&
	git merge --no-ff dev -m "Merge dev into master." &&
	git push origin master &&
	git tag "$VERSION_TAG" &&
	git push origin "$VERSION_TAG" &&
	git checkout dev
done
