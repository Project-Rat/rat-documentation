directories=(
	"../../rat-common/build" 
	"../../distmesh-cpp/build" 
	"../../materials-cpp/build"
	"../../materials-data/build"
	"../../rat-math/build"
	"../../rat-mlfmm/build"
	"../../rat-math/build"
	"../../rat-nl/build"
	"../../rat-models/build"
	"../../raccoon2/build"
	"../../rat-gui/build"
	"../../rat-template/build"
	)

for dir in "${directories[@]}"
do
	cd "$dir" &&
	git pull origin dev &&
	git add .. &&
	git commit -m "Updated cmake requirement to 3.20 to avoid depreciated warning." &&
	git push origin dev
done
