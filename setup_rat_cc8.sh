# Setup file for Rat on CentOS8 
# These commands can also be placed in .bashrc

# Author: dr. Nikkie Deelen
# Contact: nikkie.deelen@cern.ch

# Single thread MKL:
MKL_NUM_THREADS=1
