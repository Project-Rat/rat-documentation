![Logo](./figs/RATLogo.png)

# Installation sequence for Rat on Windows

<em>Rat says, Windows stinks...</em>

## Content

1. [Introduction](#introduction)
2. [Requirements](#requirements)
3. [Preparation of the build tools](#preparation-of-the-build-tools)
4. [Installation of the dependencies](#installation-of-the-dependencies)
5. [Recompile Rat from sources](#recompile-rat-from-sources)
6. [External links](#external-links)

_____

## Introduction

This guide was written to describe a procedure to recompile Rat from sources on Windows 10. The first section "[Requirements](#requirements)" will give a brief overview the tools and the libraries that we will need across the entire process. Section 3 "[Preparation of the build tools](#preparation-of-the-build-tools)" will guide you through the download and installation of the necessary software. Then, section 4 "[Installation of the dependencies](#installation-of-the-dependencies)" will explain how to use these programs to acquire and prepare the C/C++ libraries that Rat depends on, setting up the stage for the last section "[Recompile Rat from sources](#recompile-rat-from-sources)" in which we will guide you to recompile project Rat on your machine. For the sake of readbility, all links to external resources have been regrouped into an annex "[External link](#external-links)" at the end of this document.

_____

## Requirements

The [main Rat documentation](./README.md) gives lists the dependencies of Project-Rat as well as the relationships between the internal sup-projects. The following graph summarizes the parts of the information that will be relevant in this guide.

![Diagram of the dependencies for the Rat-project](./figs/rat_dependencies_schematic.png)

<<<<<<< HEAD
cd ../../rat-common/build & ninja & ninja install & cd ../../distmesh-cpp/build & ninja & ninja install & cd ../../materials-cpp/build & ninja & ninja install & cd ../../rat-mlfmm/build & ninja & ninja install & cd ../../rat-models/build & ninja & ninja install & cd ../../rat-gui/build & ninja & .\Release\bin\gui

=======
We can split the libraries Project-Rat uses between *general-purpose* (like JsonCPP, TClap or VTK) and *scientific* (BLAS, LAPACK or Armadillo). For the first ones we don't necessarilly care too much about the exact implementation we use but, for the second categories, it is much more important to know precisely the version we are using and to compile it properly for our specific harware if we want to get the best computational performances later. For this reason, this procedure will use an automated package manager to acquire the *general-purpose* libraries (right on the above schematic) while we will be recompiling the *scientific* ones (left) from source manually.

To rebuild Rat from source on Windows, these procedure will use the following software:

* **msvc** - Microsoft's C/C++ compiler & linker
* **ninja** - Compact and speed-focused build system 
* **CMake** - Kitware's build automation tool
* **vcpkg** - Microsoft's C/C++ dependency manager
* **nvcc** - Nvidia's CUDA compiler (optional)

With the exception of the optional CUDA compiler, all the software components listed above are included and intergrated in Microsoft Visual Studio, that we are therefore going to use here.

_____

## Preparation of the build tools

### (Optional) Check your GPU characteristics

**Consider this step only if you want to compile Rat with support for GPU acceleration using CUDA.**

If so, start by verifying that your system has a CUDA capable GPU with a high enough Compute Capability. Open Windows Device Manager and expend the Graphics Card section. If you have a Nvidia GPU in your system, you should see it listed here; write down its name and move forward. If you can't find a Nvidia GPU listed, you won't be able to compile Rat with GPU support.

If your system features one (or more - lucky you) Nvidia GPU(s), verify that their Compute Capability is high enough for Rat. Search for the name of your GPU in the tables available on Nvidia's website [1] and note the associated Compute Capability. Rat supports GPUs with a Compute Capability of at least 6.0. If your graphics card has a high enough Compute Capability, you can move forward. Otherwise you won't be able to compile Rat with GPU support.

Assuming your system has compatible GPU, you should first make sure to update its drivers, the latest versions of which can be downloaded from Nvidia's website [2]. Then, you will also need the Nvidia CUDA Toolkit. The latest version can be downloaded from [3]. If your GPU is on the older side, the latest version of the Toolkit might not be retro-compatible with your hardware though. In this case, you should scroll through the list of earlier versions [4] to locate one that supports a device with your Compute Capability. A table available at [5] might be useful. Once you have found a Toolkit version compatible with your GPU, download it but don't install it yet.

To finish, look in the documentation of the version of the CUDA Toolkit you downloaded to identify the minimum version of Visual Studio supported. As the time writing (July 2023), the latest CUDA Toolkit version - 12.1.1 - supports Visual Studio 2022 17.0.

### Install Visual Studio Community 2022

Download the latest version of Visual Studio Community 2022 from [6]. If you downloaded an older version of the Nvidia CUDA Toolkit during the previous step, you might have to download an earlier version of Visual Studio instead (2019 16.x for example). These are available from Microsoft's website [7] but only if you create a (free) Microsoft account.

Launch the installer and when prompted to choose the loadouts, make sure you select "Desktop development in C/C++".

![Visual Studio loadout selection](./figs/vs_loadouts.png)

In the right panel, check that the following components are selected for installation:
* **MSVC v14x - VS2022 C++ x86/x64 build tools** - This is the C/C++ compiler that we are going to use to recompile Rat (and some of its dependencies) from source.
* **CMake C++ tools for Windows** - The Rat project uses CMake to automate the configuration and build of the code. Fortunately, Visual Studio is able to read CMake project when this component is selected.

Both should be included by default within the C++ desktop loadout. Proceed to the installation of Visual Studio.

### (Optional) Install the Nvidia CUDA Toolkit

**Follow this step only if you want to compile Rat with support for GPU acceleration using CUDA. This steps concludes what was intiated in [a previous section](#optional-check-your-gpu-characteristics).**

Install now the CUDA Toolkit you downloaded earlier. If there was no mixups in the compatibilities, the Toolkit installer should detect your Visual Studio and integrate with it.

### (Optional) Install Intel Math Kernel Library

**Follow this step if your machine has an Intel CPU and you want to compile Rat using Intel-optimized math libraries (IntelMKL). Otherwise, [a later step](#mathematical-libraries) will guide you through the installation of open-source alternatives.**

Rat makes use of the Armadillo library [8] for the linear algebra operations. Armadillo itself relies on the BLAS [9] and, optionally, the SuperLU libraries [10]. Armadillo comes with its own built-in implementation of BLAS but it is highly recommanded to link against a more performant implementation of this library whenever possible, like for example OpenBLAS [11]. One alternative for computer with an Intel CPU is to use Intel's own *Math Kernel Library*, which provides high-performance implementations of both BLAS and LAPACK routine. This step explains how to install IntelMKL for those who would like to use this option.

IntelMKL is available within Intel OneAPI HPC Toolkit [12], that comes as an expension of the OneAPI Base Toolkit [13]. To get IntelMKL, therefore download both from the linked websites and install them on your machine (first the Base Toolkit, then the HPC). The installers should also detect your Visual Studio 2022 installation and integrate the products with the development environment.

### Install VCPKG

VCPKG is a free C/C++ dependencies manager allowing you to easily acquire and manage libraries for your code projects [14]. It is a command-line program that allows you to browse and download from a large registry of C/C++ packages. VCPKG supports two modes of operations:

* *Classic mode*, in which downloaded packages are installed and made available on a session basis. Packages downloaded in this mode become available to all code projects that reference this VCPKG installation.
* *Manifest mode*, in which packages are acquired on a project basis. Each code project must embark a .json manifest listing its dependencies, that VCPKG will then fetch and install in the project directory.

In our case, because we will be re-using the same external libraries several times, we prefer using the classic mode.

You might have already noticed that the default "Desktop development in C/C++" loadout of Visual Studio 2022 already embarks VCPKG. Sadly, this particular instance can only be used in manifest mode because, as a Visual Studio plug-in, VCPKG does not have ownership of the directory where it is installed. Thus, it cannot create a session-based package repository. Consequently, we will have to download and installed another instance that we can use un classic mode.

VCPKG does not use a conventional msi installer however; instead start by launching Visual Studio 2022. On the start-up prompt, select "Clone Git repository" and enter the following url: https://github.com/Microsoft/vcpkg.git. Select a folder on your computer where to clone the project. Next in this installation, we will assume you are using a path in the form:
```
<something>\project-rat\vcpkg
```
Using this kind of directory structure will ensure the following steps work automatically, otherwise you may have to change some file paths along the way.

With the Git repository cloned, close Visual Studio 2022 and open VCPKG's directory in the file explorer. Open a Powershell console into this directory, for example by holding the shift key, right-clicking into an empty space of the directory and selecting "Open a Powershell window here" in the drop-down menu. Then, in the Powershell window run the commands:
```
.\bootstrap-vcpkg.bat
.\vcpkg integrate install
```
You now have a VCPKG installation that is not limited to manifest mode. Do not close the Powershell window right now as we will start the next section by using VCPKG to retrieve most of the C/C++ libraries we need for Rat.

_____

## Installation of the dependencies

This section describes how to use the software installed previously to get and installed the C/C++ libraries used in project Rat.

### General-purpose libraries

Open a Powershell console in the directory where you installed VCPKG (or keep the one you already previously opened). Run the following commands one at a time to download and install the packages used in Rat using classic mode:
```
.\vcpkg install jsoncpp --triplet=x64-windows
.\vcpkg install tclap --triplet=x64-windows
.\vcpkg install boost-iostreams --triplet=x64-windows
.\vcpkg install boost-filesystem --triplet=x64-windows
.\vcpkg install boost-maths --triplet=x64-windows
.\vcpkg install vtk[core] --triplet=x64-windows
```
Some commands will take (a lot) longer to complete that the other, depending on the number of dependencies that the libraries have themselves. Once you have runned all the commands above, you can move to the next point. If you have already installed Intel Math Kernel Libraries previously, you can lose your Powershell console. Otherwise, keep it handy.

### Mathematical libraries

**Skip this step if you [have previously installed IntelMKL](#optional-install-intel-math-kernel-library).**

As already mentionned earlier, it is highly recommended to provide an efficient implementation of the BLAS & LAPACK mathematical library in order to recompile Armadillo [8]. In the absence of IntelMKL, we suggest relying open-source alternatives like openBLAS [11]. They can be installed using VCPKG in the same fashion as the general-purpose libraries in the previous step. Be aware though that this approach might lead to worse computational performances since the automated build handled by VCPKG might not correctly pick-up the environment.
```
.\vcpkg install openblas --triplet=x64-windows
.\vcpkg install lapack --triplet=x64-windows
```
Now that you have a version of the all the low-level mathematical libraries, the following steps will detail the steps to manually recompile from source the next libraries higher up the chain.

### Configure low-level math libraries in single-threaded mode

To get the best performances out of Rat, it is also very important to set the low -level mathematical libraries you installed (i.e. either IntelMKL or OpenBLAS) to work in *single-threaded mode*. This is because Rat already takes care of the parallelization of the computations using OpenMP threads. Therefore, we want to prevent the unerlying mathematical libraries to spawn an additionnal layer of threads that would overload our computer. Single-threaded can be enforced at runtime by setting an environement variable.

Open Windows control panel. In the search field, type "variables" and select the proposed option to "Set user environment variables". The window below will appear:

![Environment variables setup panel](./figs/envvar.png)

Click on the "New..." button located below the first list above (the list of user-specific variables). In the new pop-up window, enter "OMP_NUM_THREADS" as the variable name and "1" as its value, then hit "OK". The new variable OMP_NUM_THREADS should now appear in the top list. Repeat the same procedure to create a second environment variable that should be named either "MKL_NUM_THREADS" if you are using IntelMKL or "OPENBLAS_NUM_THREADS" if you are using OpenBLAS. In both case, the variable value is still "1". Hit "OK" again, then close environment variable panel. 

### (Optional) SuperLU

**Follow this step if you want to compile Rat with SuperLU enabled to solve large sparse linear systems.**

SuperLU (shorthand for Supernodal-LU) is a library providing support for the direct solution of large, sparse and non-symmetric systems of linear equations [15]. SuperLU is an optional dependency for the Armadillo library. It is recommanded to use it in order to maximize the performances of Rat.

We will recompile SuperLU by hand from sources using Visual Studio in order to control the build options. Open Visual Studio 2022. Either using the startup page (*Clone a git repo* option on the right menu) or by selecting *Git > clone* in the menu bar of the IDE main windows, open the following prompt:

![Visual Studio git clone prompt](./figs/vs_git_clone.png)

On the first line, enter the https URL of the git repo you want to clone [16], here https://github.com/xiaoyeli/superlu.git. On the second line below, enter the path on your local machine where you want the files to be copied. We are assuming that you are using a path consistant with what we started to used [during VCPKG installation](#install-vcpkg), namely something in the form:
```
<something>\project-rat\superlu
```
With both field filled, hit clone to proceed with downloading the sources. The main window of the IDE will then show up, with the project directory tree opened on the left. This is the time to open the *CMakeLists.txt* file and edit the generation options.

![CMakeLists.txt edition in Visual Studio 2022](./figs/vs_cmakelists.png)

Select then the desired output configuration (Debug or Release) using the drop-down menu at the top of the main window. Then, in the menu bar, hit *Project > Delete CMake cache and reconfigure*. CMake will then configure the build of the module, outputing the log messages to the integrated console at the bottom of the screen.

![CMake reconfigure command in Visual Studio 2022 menu bar](./figs/vs_reconfigure.png)

Once configuration has finished, select in the menu bar *Generate > Rebuild all* to recompile the sources. 

![Regenerate all command in Visual Studio 2022 menu bar](./figs/vs_regenerate.png)

After this is finished, and assuming you activated the option to build the test programs, you can run the those by selecting *Test > Run CTests for SuperLU*. The test results will be displayed in the integrated console.

![Run all tests command in Visual Studio 2022 menu bar](./figs/vs_run_all_tests.png)

Lastly, you can install the module you just built using the menu command *Generate > Install SuperLU*. This completes the recompilation of SuperLU module for your system.

### Armadillo

Armadillo [8] is a high quality linear algebra library (matrix maths) for the C++ language, providing a high-level syntax similar to Matlab and aiming towards a good balance between speed and ease of use. Rat makes extensive use of the Armadillo library for all aspects linear algebra.

Just as SuperLU, we propose to recompile Armadillo from sources and the procedure to do so will be more or less the same as in the previous section. We first need to get a copy of the C++ sources for Armadillo. This is done in exactly the same was as for SuperLU, using *git clone* except this time the URL for the remote repository is https://gitlab.com/conradsnicta/armadillo-code and the suggested local path to store the project is:
```
<something>/project-rat/armadillo-code
```

Configuring Armadillo is a bit different than SuperLu as the options are found in the header file:
```
<something>/project-rat/armadillo-code/include/armadillo_bits/config.hpp
```
There, you need to set which libraries are present on your system and which you want to use with Armadillo. A good template is:
```
36 #define ARMA_USE_LAPACK
43 #define ARMA_USE_BLAS
51 #define ARMA_USE_NEWARP
57 // #define ARMA_USE_ARPACK
63 #define ARMA_USE_SUPERLU <- commented if you skipped installation of SuperLU
70 #define ARMA_SUPERLU_INCLUDE_DIR <something>/project-rat/superlu/out/x64-release/include
77 // #define ARMA_USE_ATLAS
82 // #define ARMA_USE_HDF5
89 // #define ARMA_USE_FFTW3
```
Once this is done, the rest of the compilation procedure is exactely the same as for SuperLU before.

_____

## Recompile Rat from sources

As seen on the diagram in the [Requirements](#requirements) section, project Rat is split between several inter-dependant modules, that need to be compiled in a precise order:

1. **Rat-Common** - Contains common files for Rat.
2. **DistMesh-CPP** - Tri- and quad-mesh generator based on distmesh written in C++.
3. **Materials-CPP** - Implementation in C++ for using materials stored in json files.
4. **Rat-MLFMM** - Matrix-based Multi-Level Fast Multipole Method for magnetic field computations writtent in C++.
5. **Rat-Models** - Coil geometry modeller and field calculation written in C++.

Thereby, the procedure described above for the recompilation of SuperLU and Armadillo needs to be applied to every Rat module listed here in order to eventually fully rebuild project Rat on your machine. Be careful to stay consistent with the directory structure we started to make sure dependencies are found properly.

This completes this guide on the recompilation of Project-Rat for your Windows system. You should now have a working environment to make your magnetic simulations.
_____

## External links

[1]: https://developer.nvidia.com/cuda-gpus - Listing of Nvidia GPU Compute Capabilities \
[2]: https://www.nvidia.com/download/index.aspx - Download page for Nvidia GPU drivers \
[3]: https://developer.nvidia.com/cuda-downloads - Download page for the latest version of the Nvidia CUDA Toolkit \
[4]: https://developer.nvidia.com/cuda-toolkit-archive - Nvidia CUDA Toolkit archive and links to previous version documentations \
[5]: https://stackoverflow.com/questions/28932864/which-compute-capability-is-supported-by-which-cuda-versions - Table of compatibilies between CUDA Toolkit versions and hardware Compute Capability \
[6]: https://visualstudio.microsoft.com/fr/downloads/ - Download page for the latest release of Microsoft Visual Studio Community \
[7]: https://my.visualstudio.com/Downloads - Download page for earlier versions of Visual Studio (2019, 2017, etc.); requires connecting with a Microsoft account \
[8]: https://arma.sourceforge.net - Home page for the Armadillo C++ linear algebra & scientific computing library \
[9]: https://www.netlib.org/blas - Home page for the BLAS (Basic Linear Algebra Subroutines) library \
[10]: https://portal.nersc.gov/project/sparse/superlu - Home page for the SuperLU (Supernodal-LU) library \
[11]: https://www.openblas.net/ - Home page for the OpenBLAS developement project \
[12]: https://www.intel.com/content/www/us/en/developer/tools/oneapi/hpc-toolkit.html - Product page for Intel OneAPI HPC Toolkit \
[13]: https://www.intel.com/content/www/us/en/developer/tools/oneapi/base-toolkit.html - Product page for Intel OneAPI Base Toolkit \
[14]: https://vcpkg.io/en/ - Home page for the VCPKG dependency manager \
[15]: https://portal.nersc.gov/project/sparse/superlu/ - Home page for the SuperLU project \
[16]: https://gitlab.com/Project-Rat - Gitlab project page for Rat 