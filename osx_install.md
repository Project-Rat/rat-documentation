![Logo](./figs/RATLogo.png)
# Installation sequence for Rat in OSX
<em>You got one of those A(m)pple machines, shiny hmm ...</em>

## Pre-requisites
The same prerequisites for linux apply. The easiest way to install those on MacOSX is by using [MacPorts](https://www.macports.org/), which relies on [XCode](https://developer.apple.com/xcode/). In addition, [iTerm2](https://iterm2.com/) is recommended as a terminal emulator. Here is a list with all the dependencies and their installation instructions:

* [iTerm2](https://iterm2.com/) - Optional - Dowload it from their [website](https://iterm2.com/) and install with the MacOSX package manager.
* [XCode](https://developer.apple.com/xcode/) - **Required** - Needed for compilation on MacOSX. Install via the AppStore, then run the following command in iTerm2 to make sure the command line tools are up to date:
```bash
sudo xcode-select --install
sudo xcodebuild -license	# And agree to the license before installing macports
```
If you get an error when running the xcode-select command, it usually means that this is already installed and you can probably continue.
* [MacPorts](https://www.macports.org/) - **Higly recommended** - In principle the use of MacPorts is optional, however, without it you need to manually compile all the other pre-requisites. First download the right version of MacPorts via their [website](https://www.macports.org/install.php). Then you can open your terminal, for instance [iTerm2](https://iterm2.com/), and check if it is up to date:
```bash
sudo port selfupdate
sudo port upgrade outdated	# If you already have ports installed you can update them like this
``` 
Now we can use the macports to install all the required external libraries from the terminal. To install the dependencies type the following commands in your terminal. Some packages might already be installed, this is no problem.
* [git](https://git-scm.com/) - **Required** - Git version control. Install with
```bash
sudo port install git
```
* [CMake](https://cmake.org/) and [Make](https://www.gnu.org/software/make/) - **Required** - Used for compilation and installation. Install with
```bash
sudo port install cmake
```
* [Armadillo](http://arma.sourceforge.net) - **Required** - Linear algebra operations:
```bash
sudo port install armadillo
```
* [VTK](https://vtk.org/) - **Required** - Writing output files to VTK format. The simplest way to install is using MacPorts:
```bash
sudo port install vtk
```
* [Jsoncpp](http://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html) - **Required** - Json format is used for the (de-)serializing of user objects:
```bash
sudo port install jsoncpp
```
* [TClap](http://tclap.sourceforge.net) - **Required** - A command line input parser. You will need to download version 1.4 from the website, unpack it in your preferred location and according to the INSTALL fill run the following commands to install:
```bash
mkdir build 
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local .. # specify this location such that Rat can find the files
cmake --build .
sudo cmake --install .
```
* [Boost](https://www.boost.org/) - **Required** - Boost is used because it contains many handy c++ libraries. It can be installed as follows:
```bash
sudo port install boost178
```
* [CUDA](https://en.wikipedia.org/wiki/CUDA) - Optional - If you have a supported NVIDIA graphics card, 7.5 or higher, and wish to use CUDA acceleration you need to install it too:
```bash
sudo port install cuda
```
* [Paraview](https://www.paraview.org/) - **Required for Rat-Models** - Paraview for post processing. Download from their [website](https://www.paraview.org/download/), and install with the MacOS package manager. Installation of ParaView with MacPorts usually fails.

## Installing Rat

Now we will build all the libraries. The MLFMM library can make use of CUDA GPU acceleration. In the latest version CUDA is automatically detected by CMake. In case of problems this detection can be overridden by setting the CUDA flag in the cmakelists.txt file. 
```cmake
set(ENABLE_CUDA 0) # cuda disabled
set(ENABLE_CUDA 1) # cuda enabled
```

First make directory for all Rat libraries, this is optional but tidy:
```bash
mkdir project-rat
cd project-rat 			# In principle you can do this from the file browser on MacOSX as well, but Rats likes terminals :)
```

There are five different Rat libraries that need to be installed in the order that they are written in below:

* [Rat-Common](https://gitlab.com/Project-Rat/rat-common) - This library contains common files useful for all different parts of Rat. It contains, amongst other things, extensions to Armadillo, several iterative solvers, a logger and error handling.
* [DistMesh-CPP](https://gitlab.com/Project-Rat/distmesh-cpp) - A two-dimensional tri- and quad-mesher originally written in Matlab by P.-O. Persson. In contrast to most usual meshers it uses a distance function to describe the geometry. Rat has its own version of the mesher translated to C++ augmented with Armadillo.
* [Materials-CPP](https://gitlab.com/Project-Rat/materials-cpp) - Material property library based on json files with modeling integration.
* [Rat-MLFMM](https://gitlab.com/Project-Rat/rat-mlfmm) - The Multi-Level Fast Multipole Method used for calculating the vector potential and magnetic field from any collection of line current elements or magnetic moments. This is the heart of Rat and is not only useful for magneto-static coil modeling, but can for example also be used for calculating inductive voltages in a network simulation. It is therefore provided as a separate module.
* [Rat-Models](https://gitlab.com/Project-Rat/rat-models/) - This library allows for modeling coils and to calculate their magnetic fields, for example, on the surface of the coil or in a volume. Many tools related to magnet design reside here.
* [Rat-NL](https://gitlab.com/Project-Rat/rat-nl) - This is the repository for the future non-linear materials solver, which is not yet implemented. The idea is to build an A-edge integral method, using the MLFMM to avoid the fully dense matrix, usually associated with integral methods.

All of them can be installed as follows where <git repo> needs to be replace with the address of the library that you are installing, and <git repo name> by the name of the library. For Rat-Common <git repo> is https://gitlab.com/Project-Rat/rat-common.git, and <git repo name> is rat-common. ***If you are a developper, please make a fork and clone your own fork.***

```bash
git clone <git repo>
cd <git repo name>
git fetch
git checkout -b dev dev/origin
mkdir build && cd build
# cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local -DBoost_INCLUDE_DIR=/opt/local/libexec/boost//1.78/include .. # Change the location of boost if it was not installed in this directory
make -j8
make test	# Make sure all tests pass, rat-materials has no tests
sudo make install
```

It is very important that you specify the install prefix for the MacOSX installation to be **/opt/local** like in the example code. When looking at the git page for those individual libraries, you'll find a blue button on the right labeled "clone". If you click it, you'll see the addresses that can be used to clone the repo. So for the first library, Rat Common, execute the following commands:
```bash
git clone https://gitlab.com/Project-Rat/rat-common.git 	# For general use, the https address is selected. For advanced use you choose the ssh option
cd rat-common 			# When you type 'ls' in your terminal you should see this directory has been added after the git clone command
git fetch				# To make sure all the upstream branches of the repo are fetched by git
git checkout -b dev dev/origin 	# Checkout the dev branch which contains the latest and greates of the code.
mkdir build && cd build		# Create directories for compiling the code, Rat does not support in source build.
# cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local ..
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/local -DBoost_INCLUDE_DIR=/opt/local/libexec/boost//1.78/include .. # Change the location of boost if it was not installed in this directory
make -j8	# Compile the code
make test	# Make sure all tests pass if not, you might miss some of the dependencies
sudo make install 	# Finally install Rat so that you can run it from anywhere in you system.
```

## Extra: Setting up Sublime
Install the sublime [package control](https://packagecontrol.io/installation) by opening the "tools" menu and select "install package control". We can now use the package control to install the following packages. 
* Darkmatter color scheme (Enable it in sublime text -> preferences -> color scheme -> darkmatter and sublime text -> preferences -> theme -> Adaptive.sublime theme)
* Cuda C++
* A file icon
* Cmake
* SideBarEnhancements
* BracketHighLighter