![Logo](./figs/RATLogo.png)

# Installation sequence for Rat in CentOS 8 (CERN image)
<em>Rat thinks Linux is best choice for development ... yes ...</em>

This installation guide was written for installing Rat on CentOS 8 CERN (CC8) [WITHOUT GPU](#todo), and might not be applicable to non-CERN CentOS 8. A brief guide for installing CentOS 8 on you CERN pc is given [here](https://linux.web.cern.ch/install/). 
*Last update on 06/10/2020*

## Pre-requisites
The same [pre-requisites](https://gitlab.com/Project-Rat/rat-documentation/-/blob/master/README.md) for Rat are needed on CC8 as for other operating systems. This guide will specifically show how to install those and Rat on CC8.

* [git](https://git-scm.com/) - **Required** - Git version control. Install with
```bash
sudo yum -y install git 	# If Git is not already installed
```
* [gcc](https://gcc.gnu.org/) - **Required** - C++ compiler. Gcc 8.3.1 is standard on CentOS 8 CERN.
```bash
sudo yum -y install gcc		# If gcc is not already installed
```
* [CMake](https://cmake.org/) and [Make](https://www.gnu.org/software/make/) - **Required** - Installation. CMake 3.11.4 and Make 4.2.1 are standard on CentOS 8 CERN.
```bash
sudo yum -y install cmake make	# If CMake and Make are not already installed
```
* [Armadillo](http://arma.sourceforge.net) - **Required** - Linear algebra operations, needs [LaPACK](http://www.netlib.org/lapack/) and [MKL](https://software.intel.com/content/www/us/en/develop/articles/installing-intel-free-libs-and-python-yum-repo.html):
First install LaPack:
```bash
sudo yum -y install lapack lapack-devel
```
Then MKL:
```bash
sudo yum-config-manager --add-repo https://yum.repos.intel.com/mkl/setup/intel-mkl.repo
sudo yum update
sudo yum -y install intel-mkl-64bit-2020.3-111
```
Then make sure the library path is updated, and set the MKL number of threads to 1:
```bash
LD_LIBRARY_PATH=/opt/intel/mkl/lib/intel64/	# Or add export PATH=$PATH:/opt/intel/mkl/lib/intel64 to you .bashrc
MKL_NUM_THREADS=1
```
Then make armadillo from scratch (need version 9.900.x due to CMake version 3.11.4):
```bash
git clone https://gitlab.com/conradsnicta/armadillo-code.git armadillo
cd armadillo
git fetch
git checkout -b 9.900.x origin/9.900.x
mkdir build && cd build
cmake ..
make -j6
sudo make install
```
* [VTK](https://vtk.org/) - **Required** - Writing output files to VTK format. First MPICH is install which is used by VTK:
```bash
sudo yum -y install mpich
```
Then VTK is cloned and version 9.0.1 selected (due to CMake version 3.11.4):
```bash
git clone https://gitlab.kitware.com/vtk/vtk.git vtk
cd vtk
git fetch
git checkout tags/v9.0.1 -b v9.0.1
```
Then VTK is build:
```bash
mkdir build && cd build
cmake ..
make -j8
sudo make install
```
* [Jsoncpp](http://open-source-parsers.github.io/jsoncpp-docs/doxygen/index.html) - **Required** - Json format is used for (de-)serializing of user objects:
```bash
sudo yum -y install jsoncpp jsoncpp-devel
```
* [TClap](http://tclap.sourceforge.net) - **Required** - A command line input parser.
First the source is downloaded and unpacked:
```bash
wget https://sourceforge.net/projects/tclap/files/tclap-1.2.2.tar.gz
tar -xvf tclap-1.2.2.tar.gz
```
Then it can be installed:
```bash
cd tclap-1.2.2/
./configure
make -j8
sudo make install
```
* [Boost](https://www.boost.org/) - **Required** - Boost is used for instance for filesystem. It can be installed as follows:
```bash
sudo yum -y install boost boost-devel
```
* [Paraview](https://www.paraview.org/) - **Required for Rat-Models** - Paraview for post processing. First obtain the source:
```bash
git clone https://gitlab.kitware.com/paraview/paraview.git paraview
cd paraview
git checkout tags/v5.8.1 -b v5.8.1
git submodule update --init --recursive
```
Now paraview can be build. Make sure to install it in /opt/ to avoid clashes with the already installed VTK:
```bash
mkdir build
cd build
sudo mkdir /opt/paraview
cmake -DCMAKE_INSTALL_PREFIX=/opt/paraview/ -DCMAKE_BUILD_TYPE=Release ..
make -j8
sudo make install
```

## Installing Rat
Make directory for all Rat libraries, this is optional but tidy:
```bash
mkdir project-rat
cd project-rat
```

There are five different Rat libraries that need to be installed in the order below. All of them can be installed as follows:
```bash
git clone <git repo>
cd <git repo>
git fetch
git checkout -b dev dev/Origin
mkdir build && cd build
cmake -DBLA_VENDOR=Intel10_64lp_seq ..
make -j8
make test	# Make sure all tests pass, materials has no tests
sudo make install
```

### Rat libraries installation order
The installation order is the following:

* [Rat-Common](https://gitlab.com/Project-Rat/rat-common) - This library contains common files useful for all different parts of Rat. It contains, amongst other things, extensions to Armadillo, several iterative solvers, a logger and error handling.
* [DistMesh-CPP](https://gitlab.com/Project-Rat/distmesh-cpp) - A two-dimensional tri- and quad-mesher originally written in Matlab by P.-O. Persson. In contrast to most usual meshers it uses a distance function to describe the geometry. Rat has its own version of the mesher translated to C++ augmented with Armadillo.
* [Materials-CPP](https://gitlab.com/Project-Rat/materials-cpp) - Material property library based on json files with modeling integration.
* [Rat-MLFMM](https://gitlab.com/Project-Rat/rat-mlfmm) - The Multi-Level Fast Multipole Method used for calculating the vector potential and magnetic field from any collection of line current elements or magnetic moments. This is the heart of Rat and is not only useful for magneto-static coil modeling, but can for example also be used for calculating inductive voltages in a network simulation. It is therefore provided as a separate module.
* [Rat-Models](https://gitlab.com/Project-Rat/rat-models/) - This library allows for modeling coils and to calculate their magnetic fields, for example, on the surface of the coil or in a volume. Many tools related to magnet design reside here.
* [Rat-NL](https://gitlab.com/Project-Rat/rat-nl) - This is the repository for the future non-linear materials solver, which is not yet implemented. The idea is to build an A-edge integral method, using the MLFMM to avoid the fully dense matrix, usually associated with integral methods.

### Using Rat
An example API has been prepared in [Rat-Template](https://gitlab.com/Project-Rat/rat-template). It includes an example of a solenoid, and how to link it to the other Rat libraries. To run it, compile in a build directory just like the other [Rat libraries](#rat-installation). This generates a solenoid example program, which can executed in the terminal from the build directory:
```bash
./solenoid
```
The results of the solenoid example are stored in the build/output/ directory. It can be opened with Paraview as follows:
```bash
paraview output/solenoid_msh.vtu
```

## ToDo
* Add information about CUDA to this manual (this has not been tested yet!)

## Authors
Dr. Nikkie Deelen

## License
This project is licensed under the [GNU public license](LICENSE).
